<?php

// https://paulund.co.uk/add-new-menu-items-to-wordpress-admin
// NB: add_theme_page() function is used when we want to show new page to Appearance.
// & add_options_page() function is used when we want to show new page to Settings.
// & add_menu_page() function is used when we want to show new page with new menu
// Both functions use same parameters
// add_submenu_page() function is used to create sub-menu under a menu

function newtheme_init()
{
	add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'newtheme_init' );

// Working with Settings API
function registerFields()
{
	add_settings_section( 'header_options', 'Header Options', 'headerOptionsContainer', 'new_theme_options' );

	add_settings_section( 'social_options', 'Social Options', 'socialOptionsContainer', 'new_theme_options' );

	add_settings_field( 'header_text', 'Header Text', 'headerContainer', 'new_theme_options', 'header_options' );
	add_settings_field( 'copyright_text', 'Copyright Text', 'copyrightContainer', 'new_theme_options', 'header_options' );
	add_settings_field( 'facebook_link', 'Facebook', 'facebookContainer', 'new_theme_options', 'social_options' );

	register_setting( 'header_options', 'header_text' );
	register_setting( 'header_options', 'copyright_text' );
	register_setting( 'header_options', 'facebook_link' );
}
add_action( 'admin_init', 'registerFields' );

function headerContainer()
{
	echo '<input type="text" class="regular-text" name="header_text" value="'.get_option( 'header_text' ).'"/>';
}

function copyrightContainer()
{
	echo '<input type="text" class="regular-text" name="copyright_text" value="'.get_option( 'copyright_text' ).'"/>';
}

function facebookContainer()
{
	echo '<input type="text" class="regular-text" name="facebook_link" value="'.esc_url(get_option( 'facebook_link' )).'"/>';
}


// add new menu item
function register_menu()
{
	add_menu_page( 'Theme Options', 'Theme Options', 'manage_options', 'new_theme_options', 'themeOptionsContainer', 'dashicons-menu', 48 );
}
add_action( 'admin_menu', 'register_menu' );

function headerOptionsContainer()
{
	echo 'Add or Update your header options.';
}

function socialOptionsContainer()
{
	echo 'Provide social media to get connected with your clients.';
}

function themeOptionsContainer()
{
	?>
	
	<h2>Theme Options</h2>

	<?php settings_errors() ?>

	<form action="options.php" method="POST">
		
		<?php do_settings_sections( 'new_theme_options' ); ?>

		<?php settings_fields( 'header_options' ); ?>

		<?php submit_button(); ?>
	</form>

	<?php
}

// work with API settings on another section
if(file_exists(__FILE__).'/biography-options.php')
{
	require_once 'biography-options.php';
}

if(file_exists(__FILE__).'/demo.php')
{
	require_once 'demo.php';
}
