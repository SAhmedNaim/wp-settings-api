<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>





	<?php echo get_option('header_text'); ?>
	<hr/>

	<?php echo get_option('copyright_text'); ?>
	<hr/>

	<?php echo get_option('facebook_link'); ?>

	<hr>
	<h2>Biography</h2>
	<hr>

	<p>
		Name: 
		<?php 
			$value = (array)get_option('data');
			echo $value['biography_name'];	
		?>
	</p>

	<p>
		Profession: 
		<?php 
			$value = (array)get_option('data');
			echo $value['biography_profession'];	
		?>
		</p>

	<p>
		Present Address: 
		<?php 

			$value = (array)get_option('data');
			echo $value['biography_present_address'];	
		?>
	</p>
	
	<?php 
		$value = (array)get_option('data');
		$biography_dob_show = $value['biography_dob_show'];

		if($biography_dob_show) :

	?>

		<p>
			Date of Birth: 
			<?php 

				$value = (array)get_option('data');
				echo $value['biography_dob'];	
			?>
		</p>

	<?php endif; ?>

	<p>
		School: 
		<?php 

			$value = (array)get_option('data');
			echo $value['education_school'];	
		?>			
	</p>

	<p>
		College: 
		<?php 
			$value = (array)get_option('data');
			echo $value['education_college'];	
		?>	
	</p>

	<p>
		University: 
		<?php 
			$value = (array)get_option('data');
			echo $value['education_university'];	
		?>				
	</p>

	<p>
		Favourite Programming Language: 
		<?php 
			$value = (array)get_option('data');
			$education_programming_language = $value['education_programming_language'];
			echo $education_programming_language;
		?>
	</p>

	
	<?php wp_footer(); ?>
</body>
</html>