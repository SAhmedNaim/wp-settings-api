<?php

function demo_settings()
{
    add_settings_section("section", "Section", null, "demo");
    add_settings_field("demo-select", "Demo Select Box", "demo_select_display", "demo", "section");  
    register_setting("section", "demo-select");
}

function demo_select_display()
{
   ?>
        <select name="demo-select">
          <option value="qscutter" <?php selected(get_option('demo-select'), "qscutter"); ?>>QScutter</option>
          <option value="qnimate" <?php selected(get_option('demo-select'), "qnimate"); ?>>QNimate</option>
          <option value="qidea" <?php selected(get_option('demo-select'), "qidea"); ?>>QIdea</option>
          <option value="qtrack" <?php selected(get_option('demo-select'), "qtrack"); ?>>QTrack</option>
        </select>
   <?php
}

add_action("admin_init", "demo_settings");

function demo_page()
{
  ?>
      <div class="wrap">
         <h1>Demo</h1>
  
         <form method="post" action="options.php">
            <?php
               settings_fields("section");
  
               do_settings_sections("demo");
                 
               submit_button(); 
            ?>
         </form>
      </div>
   <?php
}

function menu_item()
{
  add_submenu_page("options-general.php", "Demo", "Demo", "manage_options", "demo", "demo_page"); 
}
 
add_action("admin_menu", "menu_item");