<?php

function theme_option_biography()
{
	// Introduction Section
	add_settings_section( 'introduction', 'Introduction', 'introductionContainer', 'biography.php' );

	add_settings_field( 'biography_name', 'Name', 'nameContainer', 'biography.php', 'introduction' );
	add_settings_field( 'biography_profession', 'Profession', 'professionContainer', 'biography.php', 'introduction' );
	add_settings_field( 'biography_present_address', 'Present Address', 'presentAddressContainer', 'biography.php', 'introduction' );
	add_settings_field( 'biography_dob', 'Date of Birth', 'dobContainer', 'biography.php', 'introduction' );
	add_settings_field( 'biography_dob_show', 'Show Date of Birth', 'dobShowContainer', 'biography.php', 'introduction' );

	register_setting( 'introduction', 'data' );
	register_setting( 'introduction', 'data' );
	register_setting( 'introduction', 'data' );
	register_setting( 'introduction', 'data' );
	register_setting( 'introduction', 'data' );

	// Education Section
	add_settings_section( 'education', 'Education', 'educationContainer', 'biography.php' );

	add_settings_field( 'education_school', 'School Name', 'schoolContainer', 'biography.php', 'education' );
	add_settings_field( 'education_college', 'College Name', 'collegeContainer', 'biography.php', 'education' );
	add_settings_field( 'education_university', 'University Name', 'universityContainer', 'biography.php', 'education' );
	

	register_setting( 'education', 'data' );
	register_setting( 'education', 'data' );
	register_setting( 'education', 'data' );


}
add_action( 'admin_init', 'theme_option_biography' );

function introductionContainer()
{
	echo 'Add to introduce youself.<hr/>';
}

function nameContainer()
{
	$value = (array)get_option('data');
	$biography_name = $value['biography_name'];

	echo '<input type="text" class="regular-text" placeholder="Enter name here..." name="data[biography_name]" value="'.$biography_name.'" required />';
}

function professionContainer()
{
	$value = (array)get_option('data');
	$biography_profession = $value['biography_profession'];

	echo '<input type="text" class="regular-text" placeholder="Enter profession here..." name="data[biography_profession]" value="'.$biography_profession.'" required />';
}

function presentAddressContainer()
{
	$value = (array)get_option('data');
	$biography_present_address = $value['biography_present_address'];

	echo '<input type="text" class="regular-text" placeholder="Enter presnt address here..." name="data[biography_present_address]" value="'.$biography_present_address.'" required />';
}

function dobContainer()
{
	$value = (array)get_option('data');
	$biography_dob = $value['biography_dob'];

	echo '<input type="date" class="regular-text" placeholder="Enter Date of Birth here..." name="data[biography_dob]" value="'.$biography_dob.'" required />';
}

function dobShowContainer()
{
	$value = (array)get_option('data');
	$biography_dob_show = $value['biography_dob_show'];

	echo '<input type="checkbox" class="regular-text" placeholder="Enter Date of Birth here..." name="data[biography_dob_show]" value="1" '.checked( 1, $biography_dob_show, false ).' />';
}





function educationContainer()
{
	echo 'Add your educational qualification here.<hr/>';
}

function schoolContainer()
{
	$value = (array)get_option('data');
	$education_school = $value['education_school'];

	echo '<input type="text" class="regular-text" placeholder="Enter school name here..." name="data[education_school]" value="'.$education_school.'" required />';
}

function collegeContainer()
{
	$value = (array)get_option('data');
	$education_college = $value['education_college'];

	echo '<input type="text" class="regular-text" placeholder="Enter college name here..." name="data[education_college]" value="'.$education_college.'" required />';
}

function universityContainer()
{
	$value = (array)get_option('data');
	$education_university = $value['education_university'];

	echo '<input type="text" class="regular-text" placeholder="Enter university name here..." name="data[education_university]" value="'.$education_university.'" required />';
}





function theme_options_as_submenu()
{
	add_theme_page( 'Biography', 'Biography', 'manage_options', 'biography.php', 'containerBiography' );
}
add_action( 'admin_menu', 'theme_options_as_submenu' );

function containerBiography()
{
	?>
	
	<div class="wrap">
		
		<h2>Biography</h2>
		
		<?php settings_errors(); ?>
		
		<form action="options.php" method="POST">
			
			<?php do_settings_sections( 'biography.php' ); ?>

			<?php settings_fields( 'introduction' ); ?>

			<?php settings_fields( 'education' ); ?>

			
			<?php submit_button(); ?>

		</form>

	</div>


	<?php
}

